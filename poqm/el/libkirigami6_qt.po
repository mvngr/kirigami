# Stelios <sstavra@gmail.com>, 2017, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2021-10-06 10:25+0300\n"
"Last-Translator: Stelios <sstavra@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Qt-Contexts: true\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.04.2\n"

#: controls/AboutItem.qml:139
#, qt-format
msgctxt "AboutItem|"
msgid "%1 (%2)"
msgstr ""

#: controls/AboutItem.qml:148
#, qt-format
msgctxt "AboutItem|"
msgid "Send an email to %1"
msgstr "Αποστολή μηνύματος στο %1"

#: controls/AboutItem.qml:198
msgctxt "AboutItem|"
msgid "Get Involved"
msgstr "Ασχοληθείτε"

#: controls/AboutItem.qml:204
msgctxt "AboutItem|"
msgid "Donate"
msgstr ""

#: controls/AboutItem.qml:221
#, fuzzy
#| msgctxt "AboutPage|"
#| msgid "Report Bug…"
msgctxt "AboutItem|"
msgid "Report a Bug"
msgstr "Αναφορά σφάλματος…"

#: controls/AboutItem.qml:234
msgctxt "AboutItem|"
msgid "Copyright"
msgstr "Copyright"

#: controls/AboutItem.qml:278
msgctxt "AboutItem|"
msgid "License:"
msgstr "Άδεια χρήσης:"

#: controls/AboutItem.qml:300
#, qt-format
msgctxt "AboutItem|"
msgid "License: %1"
msgstr "Άδεια χρήσης: %1"

#: controls/AboutItem.qml:311
msgctxt "AboutItem|"
msgid "Libraries in use"
msgstr "Βιβλιοθήκες σε χρήση"

#: controls/AboutItem.qml:341
msgctxt "AboutItem|"
msgid "Authors"
msgstr "Συγγραφείς"

#: controls/AboutItem.qml:350
msgctxt "AboutItem|"
msgid "Show author photos"
msgstr "Εμφάνιση φωτογραφιών συγγραφέων"

#: controls/AboutItem.qml:381
msgctxt "AboutItem|"
msgid "Credits"
msgstr "Ευχαριστίες"

#: controls/AboutItem.qml:394
msgctxt "AboutItem|"
msgid "Translators"
msgstr "Μεταφραστές"

#: controls/AboutPage.qml:93
#, qt-format
msgctxt "AboutPage|"
msgid "About %1"
msgstr "Περίγραμμα %1"

#: controls/AbstractApplicationWindow.qml:204
msgctxt "AbstractApplicationWindow|"
msgid "Quit"
msgstr "Έξοδος"

#: controls/ActionToolBar.qml:192
msgctxt "ActionToolBar|"
msgid "More Actions"
msgstr "Περισσότερες ενέργειες"

#: controls/Avatar.qml:177
#, qt-format
msgctxt "Avatar|"
msgid "%1 — %2"
msgstr "%1 — %2"

#: controls/Chip.qml:82
msgctxt "Chip|"
msgid "Remove Tag"
msgstr ""

#: controls/ContextDrawer.qml:66
msgctxt "ContextDrawer|"
msgid "Actions"
msgstr "Ενέργειες"

#: controls/GlobalDrawer.qml:504
msgctxt "GlobalDrawer|"
msgid "Back"
msgstr "Πίσω"

#: controls/GlobalDrawer.qml:597
msgctxt "GlobalDrawer|"
msgid "Close Sidebar"
msgstr "Κλείσιμο πλευρικής γραμμής"

#: controls/GlobalDrawer.qml:600
#, fuzzy
#| msgctxt "GlobalDrawer|"
#| msgid "Close Sidebar"
msgctxt "GlobalDrawer|"
msgid "Open Sidebar"
msgstr "Κλείσιμο πλευρικής γραμμής"

#: controls/LoadingPlaceholder.qml:54
msgctxt "LoadingPlaceholder|"
msgid "Loading…"
msgstr ""

#: controls/PasswordField.qml:42
msgctxt "PasswordField|"
msgid "Password"
msgstr "Κωδικός πρόσβασης"

#: controls/PasswordField.qml:45
#, fuzzy
#| msgctxt "PasswordField|"
#| msgid "Password"
msgctxt "PasswordField|"
msgid "Hide Password"
msgstr "Κωδικός πρόσβασης"

#: controls/PasswordField.qml:45
#, fuzzy
#| msgctxt "PasswordField|"
#| msgid "Password"
msgctxt "PasswordField|"
msgid "Show Password"
msgstr "Κωδικός πρόσβασης"

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
#, fuzzy
#| msgctxt "OverlayDrawer|"
#| msgid "Close"
msgctxt "PageRowGlobalToolBarUI|"
msgid "Close menu"
msgstr "Κλείσιμο"

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
#, fuzzy
#| msgctxt "OverlayDrawer|"
#| msgid "Open"
msgctxt "PageRowGlobalToolBarUI|"
msgid "Open menu"
msgstr "Άνοιγμα"

#: controls/SearchField.qml:94
msgctxt "SearchField|"
msgid "Search…"
msgstr "Αναζήτηση…"

#: controls/SearchField.qml:96
msgctxt "SearchField|"
msgid "Search"
msgstr "Αναζήτηση"

#: controls/SearchField.qml:107
msgctxt "SearchField|"
msgid "Clear search"
msgstr ""

#: controls/settingscomponents/CategorizedSettings.qml:40
#: controls/settingscomponents/CategorizedSettings.qml:104
msgctxt "CategorizedSettings|"
msgid "Settings"
msgstr "Ρυθμίσεις"

#: controls/settingscomponents/CategorizedSettings.qml:40
#, qt-format
msgctxt "CategorizedSettings|"
msgid "Settings — %1"
msgstr "Ρυθμίσεις — %1"

#: controls/templates/OverlayDrawer.qml:130
#, fuzzy
#| msgctxt "GlobalDrawer|"
#| msgid "Close Sidebar"
msgctxt "OverlayDrawer|"
msgid "Close drawer"
msgstr "Κλείσιμο πλευρικής γραμμής"

#: controls/templates/OverlayDrawer.qml:136
msgctxt "OverlayDrawer|"
msgid "Open drawer"
msgstr ""

#: controls/templates/private/BackButton.qml:50
msgctxt "BackButton|"
msgid "Navigate Back"
msgstr "Πλοήγηση προς τα πίσω"

#: controls/templates/private/ForwardButton.qml:27
msgctxt "ForwardButton|"
msgid "Navigate Forward"
msgstr "Πλοήγηση προς τα εμπρός"

#: controls/UrlButton.qml:34
#, qt-format
msgctxt "UrlButton|@info:whatsthis"
msgid "Open link %1"
msgstr ""

#: controls/UrlButton.qml:35
#, fuzzy
#| msgctxt "OverlayDrawer|"
#| msgid "Open"
msgctxt "UrlButton|@info:whatsthis"
msgid "Open link"
msgstr "Άνοιγμα"

#: controls/UrlButton.qml:58
msgctxt "UrlButton|"
msgid "Copy Link to Clipboard"
msgstr ""

#: settings.cpp:211
#, qt-format
msgctxt "Settings|"
msgid "KDE Frameworks %1"
msgstr "KDE Frameworks %1"

#: settings.cpp:213
#, qt-format
msgctxt "Settings|"
msgid "The %1 windowing system"
msgstr "Το %1 παραθυρικό σύστημα"

#: settings.cpp:214
#, qt-format
msgctxt "Settings|"
msgid "Qt %2 (built against %3)"
msgstr "Qt %2 (κατασκευάστηκε με βάση το %3)"

#~ msgctxt "PageTab|"
#~ msgid "Current page. Progress: %1 percent."
#~ msgstr "Τρέχουσα σελίδα. Πρόοδος: %1 τοις εκατό."

#~ msgctxt "PageTab|"
#~ msgid "Navigate to %1. Progress: %2 percent."
#~ msgstr "Πλοήγηση προς το %1. Πρόοδος: %2 τοις εκατό."

#~ msgctxt "PageTab|"
#~ msgid "Current page."
#~ msgstr "Τρέχουσα σελίδα."

#~ msgctxt "PageTab|"
#~ msgid "Navigate to %1. Demanding attention."
#~ msgstr "Πλοήγηση προς το %1. Δώστε προσοχή."

#~ msgctxt "PageTab|"
#~ msgid "Navigate to %1."
#~ msgstr "Πλοήγηση προς το %1."

#~ msgctxt "ToolBarApplicationHeader|"
#~ msgid "More Actions"
#~ msgstr "Περισσότερες ενέργειες"

#~ msgctxt "AboutItem|"
#~ msgid "Visit %1's KDE Store page"
#~ msgstr "Επισκεφθείτε τη σελίδα της αποθήκης του KDE για το %1"

#~ msgctxt "UrlButton|"
#~ msgid "Copy link address"
#~ msgstr "Αντιγραφή διεύθυνσης δεσμού"

#~ msgctxt "AboutItem|"
#~ msgid "(%1)"
#~ msgstr "(%1)"

#~ msgctxt "SearchField|"
#~ msgid "Search..."
#~ msgstr "Αναζήτηση..."

#, fuzzy
#~| msgctxt "ContextDrawer|"
#~| msgid "Actions"
#~ msgctxt "ToolBarPageHeader|"
#~ msgid "More Actions"
#~ msgstr "Ενέργειες"
