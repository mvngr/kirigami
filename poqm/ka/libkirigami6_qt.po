msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: \n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Qt-Contexts: true\n"
"X-Generator: Poedit 3.3.1\n"

#: controls/AboutItem.qml:139
#, qt-format
msgctxt "AboutItem|"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: controls/AboutItem.qml:148
#, qt-format
msgctxt "AboutItem|"
msgid "Send an email to %1"
msgstr "%1-სთვის ელფოსტის გაგზავნა"

#: controls/AboutItem.qml:198
msgctxt "AboutItem|"
msgid "Get Involved"
msgstr "შემოგვიერთდით"

#: controls/AboutItem.qml:204
msgctxt "AboutItem|"
msgid "Donate"
msgstr "შემოწირულობა"

#: controls/AboutItem.qml:221
msgctxt "AboutItem|"
msgid "Report a Bug"
msgstr "შეცდომის პატაკი"

#: controls/AboutItem.qml:234
msgctxt "AboutItem|"
msgid "Copyright"
msgstr "საავტორო უფლებები"

#: controls/AboutItem.qml:278
msgctxt "AboutItem|"
msgid "License:"
msgstr "ლიცენზია:"

#: controls/AboutItem.qml:300
#, qt-format
msgctxt "AboutItem|"
msgid "License: %1"
msgstr "ლიცენზია: %1"

#: controls/AboutItem.qml:311
msgctxt "AboutItem|"
msgid "Libraries in use"
msgstr "გამოყენებული ბიბლიოთეკები"

#: controls/AboutItem.qml:341
msgctxt "AboutItem|"
msgid "Authors"
msgstr "ავტორები"

#: controls/AboutItem.qml:350
msgctxt "AboutItem|"
msgid "Show author photos"
msgstr "ავტორის ფოტოების ჩვენება"

#: controls/AboutItem.qml:381
msgctxt "AboutItem|"
msgid "Credits"
msgstr "კრედიტები"

#: controls/AboutItem.qml:394
msgctxt "AboutItem|"
msgid "Translators"
msgstr "მთარგმნელები"

#: controls/AboutPage.qml:93
#, qt-format
msgctxt "AboutPage|"
msgid "About %1"
msgstr "%1-ის შესახებ"

#: controls/AbstractApplicationWindow.qml:204
msgctxt "AbstractApplicationWindow|"
msgid "Quit"
msgstr "დასრულება"

#: controls/ActionToolBar.qml:192
msgctxt "ActionToolBar|"
msgid "More Actions"
msgstr "მეტი ქმედება"

#: controls/Avatar.qml:177
#, qt-format
msgctxt "Avatar|"
msgid "%1 — %2"
msgstr "%1 — %2"

#: controls/Chip.qml:82
msgctxt "Chip|"
msgid "Remove Tag"
msgstr "ჭდის წაშლა"

#: controls/ContextDrawer.qml:66
msgctxt "ContextDrawer|"
msgid "Actions"
msgstr "ქმედებები"

#: controls/GlobalDrawer.qml:504
msgctxt "GlobalDrawer|"
msgid "Back"
msgstr "უკან"

#: controls/GlobalDrawer.qml:597
msgctxt "GlobalDrawer|"
msgid "Close Sidebar"
msgstr "გვერდითი ზოლის დახურვა"

#: controls/GlobalDrawer.qml:600
msgctxt "GlobalDrawer|"
msgid "Open Sidebar"
msgstr "გვერდითი ზოლის გახსნა"

#: controls/LoadingPlaceholder.qml:54
msgctxt "LoadingPlaceholder|"
msgid "Loading…"
msgstr "ჩატვირთვა…"

#: controls/PasswordField.qml:42
msgctxt "PasswordField|"
msgid "Password"
msgstr "პაროლი"

#: controls/PasswordField.qml:45
msgctxt "PasswordField|"
msgid "Hide Password"
msgstr "პაროლის დამალვა"

#: controls/PasswordField.qml:45
msgctxt "PasswordField|"
msgid "Show Password"
msgstr "პაროლის ჩვენება"

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
msgctxt "PageRowGlobalToolBarUI|"
msgid "Close menu"
msgstr "მენიუს დახურვა"

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
msgctxt "PageRowGlobalToolBarUI|"
msgid "Open menu"
msgstr "მენიუს გახსნა"

#: controls/SearchField.qml:94
msgctxt "SearchField|"
msgid "Search…"
msgstr "ძებნა…"

#: controls/SearchField.qml:96
msgctxt "SearchField|"
msgid "Search"
msgstr "ძებნა"

#: controls/SearchField.qml:107
msgctxt "SearchField|"
msgid "Clear search"
msgstr "ძიების გასუფთავება"

#: controls/settingscomponents/CategorizedSettings.qml:40
#: controls/settingscomponents/CategorizedSettings.qml:104
msgctxt "CategorizedSettings|"
msgid "Settings"
msgstr "&მორგება"

#: controls/settingscomponents/CategorizedSettings.qml:40
#, qt-format
msgctxt "CategorizedSettings|"
msgid "Settings — %1"
msgstr "პარამეტრები — %1"

#: controls/templates/OverlayDrawer.qml:130
msgctxt "OverlayDrawer|"
msgid "Close drawer"
msgstr "უჯრის დახურვა"

#: controls/templates/OverlayDrawer.qml:136
msgctxt "OverlayDrawer|"
msgid "Open drawer"
msgstr "უჯრის გახსნა"

#: controls/templates/private/BackButton.qml:50
msgctxt "BackButton|"
msgid "Navigate Back"
msgstr "უკან გადასვლა"

#: controls/templates/private/ForwardButton.qml:27
msgctxt "ForwardButton|"
msgid "Navigate Forward"
msgstr "წინ გადასვლა"

#: controls/UrlButton.qml:34
#, qt-format
msgctxt "UrlButton|@info:whatsthis"
msgid "Open link %1"
msgstr "ბმულის გახსნა %1"

#: controls/UrlButton.qml:35
msgctxt "UrlButton|@info:whatsthis"
msgid "Open link"
msgstr "ბმულის გახსნა"

#: controls/UrlButton.qml:58
msgctxt "UrlButton|"
msgid "Copy Link to Clipboard"
msgstr "ბმულის ბმულის ბაფერში კოპირება"

#: settings.cpp:211
#, qt-format
msgctxt "Settings|"
msgid "KDE Frameworks %1"
msgstr "KDE Frameworks %1"

#: settings.cpp:213
#, qt-format
msgctxt "Settings|"
msgid "The %1 windowing system"
msgstr "ფანჯრული სისტემა: %1"

#: settings.cpp:214
#, qt-format
msgctxt "Settings|"
msgid "Qt %2 (built against %3)"
msgstr "Qt %2 (აგებულია %3-ით)"

#~ msgctxt "PageTab|"
#~ msgid "Current page. Progress: %1 percent."
#~ msgstr "მიმდინარე გვერდი. პროგრესი: %1 პროცენტი."

#~ msgctxt "PageTab|"
#~ msgid "Navigate to %1. Progress: %2 percent."
#~ msgstr "%1-ზეგადასვლა. პროგრესი: %2 პროცენტი."

#~ msgctxt "PageTab|"
#~ msgid "Current page."
#~ msgstr "მიმდინარე გვერდი."

#~ msgctxt "PageTab|"
#~ msgid "Navigate to %1. Demanding attention."
#~ msgstr "%1-ზე გადასვლა. საჭიროა ყურადღება."

#~ msgctxt "PageTab|"
#~ msgid "Navigate to %1."
#~ msgstr "%1-ზე გადასვლა."

#~ msgctxt "ToolBarApplicationHeader|"
#~ msgid "More Actions"
#~ msgstr "მეტი ქმედება"

#~ msgctxt "AboutItem|"
#~ msgid "Visit %1's KDE Store page"
#~ msgstr "ეწვიეთ %1-ის KDE მაღაზიის გვერდს"
