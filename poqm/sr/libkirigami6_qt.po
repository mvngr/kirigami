# Translation of libkirigami2plugin_qt.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: libkirigami2plugin_qt\n"
"PO-Revision-Date: 2017-10-06 17:14+0200\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: qtrich\n"
"X-Environment: kde\n"
"X-Qt-Contexts: true\n"

#: controls/AboutItem.qml:139
#, qt-format
msgctxt "AboutItem|"
msgid "%1 (%2)"
msgstr ""

#: controls/AboutItem.qml:148
#, qt-format
msgctxt "AboutItem|"
msgid "Send an email to %1"
msgstr ""

#: controls/AboutItem.qml:198
msgctxt "AboutItem|"
msgid "Get Involved"
msgstr ""

#: controls/AboutItem.qml:204
msgctxt "AboutItem|"
msgid "Donate"
msgstr ""

#: controls/AboutItem.qml:221
msgctxt "AboutItem|"
msgid "Report a Bug"
msgstr ""

#: controls/AboutItem.qml:234
msgctxt "AboutItem|"
msgid "Copyright"
msgstr ""

#: controls/AboutItem.qml:278
msgctxt "AboutItem|"
msgid "License:"
msgstr ""

#: controls/AboutItem.qml:300
#, qt-format
msgctxt "AboutItem|"
msgid "License: %1"
msgstr ""

#: controls/AboutItem.qml:311
msgctxt "AboutItem|"
msgid "Libraries in use"
msgstr ""

#: controls/AboutItem.qml:341
msgctxt "AboutItem|"
msgid "Authors"
msgstr ""

#: controls/AboutItem.qml:350
msgctxt "AboutItem|"
msgid "Show author photos"
msgstr ""

#: controls/AboutItem.qml:381
msgctxt "AboutItem|"
msgid "Credits"
msgstr ""

#: controls/AboutItem.qml:394
msgctxt "AboutItem|"
msgid "Translators"
msgstr ""

#: controls/AboutPage.qml:93
#, qt-format
msgctxt "AboutPage|"
msgid "About %1"
msgstr ""

#: controls/AbstractApplicationWindow.qml:204
msgctxt "AbstractApplicationWindow|"
msgid "Quit"
msgstr ""

#: controls/ActionToolBar.qml:192
#, fuzzy
#| msgctxt "ContextDrawer|"
#| msgid "Actions"
msgctxt "ActionToolBar|"
msgid "More Actions"
msgstr "Радње"

#: controls/Avatar.qml:177
#, qt-format
msgctxt "Avatar|"
msgid "%1 — %2"
msgstr ""

#: controls/Chip.qml:82
msgctxt "Chip|"
msgid "Remove Tag"
msgstr ""

#: controls/ContextDrawer.qml:66
msgctxt "ContextDrawer|"
msgid "Actions"
msgstr "Радње"

#: controls/GlobalDrawer.qml:504
msgctxt "GlobalDrawer|"
msgid "Back"
msgstr "Назад"

#: controls/GlobalDrawer.qml:597
msgctxt "GlobalDrawer|"
msgid "Close Sidebar"
msgstr ""

#: controls/GlobalDrawer.qml:600
msgctxt "GlobalDrawer|"
msgid "Open Sidebar"
msgstr ""

#: controls/LoadingPlaceholder.qml:54
msgctxt "LoadingPlaceholder|"
msgid "Loading…"
msgstr ""

#: controls/PasswordField.qml:42
msgctxt "PasswordField|"
msgid "Password"
msgstr ""

#: controls/PasswordField.qml:45
msgctxt "PasswordField|"
msgid "Hide Password"
msgstr ""

#: controls/PasswordField.qml:45
msgctxt "PasswordField|"
msgid "Show Password"
msgstr ""

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
msgctxt "PageRowGlobalToolBarUI|"
msgid "Close menu"
msgstr ""

#: controls/private/globaltoolbar/PageRowGlobalToolBarUI.qml:70
msgctxt "PageRowGlobalToolBarUI|"
msgid "Open menu"
msgstr ""

#: controls/SearchField.qml:94
msgctxt "SearchField|"
msgid "Search…"
msgstr ""

#: controls/SearchField.qml:96
msgctxt "SearchField|"
msgid "Search"
msgstr ""

#: controls/SearchField.qml:107
msgctxt "SearchField|"
msgid "Clear search"
msgstr ""

#: controls/settingscomponents/CategorizedSettings.qml:40
#: controls/settingscomponents/CategorizedSettings.qml:104
msgctxt "CategorizedSettings|"
msgid "Settings"
msgstr ""

#: controls/settingscomponents/CategorizedSettings.qml:40
#, qt-format
msgctxt "CategorizedSettings|"
msgid "Settings — %1"
msgstr ""

#: controls/templates/OverlayDrawer.qml:130
msgctxt "OverlayDrawer|"
msgid "Close drawer"
msgstr ""

#: controls/templates/OverlayDrawer.qml:136
msgctxt "OverlayDrawer|"
msgid "Open drawer"
msgstr ""

#: controls/templates/private/BackButton.qml:50
msgctxt "BackButton|"
msgid "Navigate Back"
msgstr "Иди назад"

#: controls/templates/private/ForwardButton.qml:27
msgctxt "ForwardButton|"
msgid "Navigate Forward"
msgstr "Иди напред"

#: controls/UrlButton.qml:34
#, qt-format
msgctxt "UrlButton|@info:whatsthis"
msgid "Open link %1"
msgstr ""

#: controls/UrlButton.qml:35
msgctxt "UrlButton|@info:whatsthis"
msgid "Open link"
msgstr ""

#: controls/UrlButton.qml:58
msgctxt "UrlButton|"
msgid "Copy Link to Clipboard"
msgstr ""

#: settings.cpp:211
#, qt-format
msgctxt "Settings|"
msgid "KDE Frameworks %1"
msgstr ""

#: settings.cpp:213
#, qt-format
msgctxt "Settings|"
msgid "The %1 windowing system"
msgstr ""

#: settings.cpp:214
#, qt-format
msgctxt "Settings|"
msgid "Qt %2 (built against %3)"
msgstr ""

#, fuzzy
#~| msgctxt "ForwardButton|"
#~| msgid "Navigate Forward"
#~ msgctxt "PageTab|"
#~ msgid "Navigate to %1."
#~ msgstr "Иди напред"

#, fuzzy
#~| msgctxt "ContextDrawer|"
#~| msgid "Actions"
#~ msgctxt "ToolBarApplicationHeader|"
#~ msgid "More Actions"
#~ msgstr "Радње"
